# FoundryVTT DND5E 中文合集

本MOD为FVTT的DND5E系统加入了资料补全，包括SRD之外的内容。

**使用说明：**

* 1. 首先在安装界面MOD安装中，安装本MOD。（Manifest: ）
* 2. 进入游戏世界（DND5E），在【设置-管理MOD】中勾选本MOD。
* 3. 在【设置-语言】中选择【中文】。
* 4. 重新载入

**手动安装方法**

* 1.在右上方选择Download-zip
* 2.解压文件，将dnd5e_chn拷贝到Data/Module
* 3.从使用说明第二步开始

遇到有什么问题可以到QQ群：716573917 交流。



